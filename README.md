[![Coverage Status](https://coveralls.io/repos/bitbucket/fcsilvaa/markdowndemo/badge.svg?branch=master)](https://coveralls.io/bitbucket/fcsilvaa/markdowndemo?branch=master)
[![Build Status](https://bitbucket-badges.atlassian.io/badge/fcsilvaa/markdowndemo.svg)](https://bitbucket.org/fcsilvaa/markdowndemo)

# Template Sync

This project is used to sync templates to SendGrid plataform through RabbitMQ and SendGrid's API.

|SendGrid - API | RabbitMQ - Tutorial|
|:-------------:|:------------------:|
|[![SendGrid](https://avatars3.githubusercontent.com/u/181234?s=200&v=4 "SendGrid - API")](https://sendgrid.com/docs/API_Reference/api_v3.html)|[![RabbitMQ](https://d1q6f0aelx0por.cloudfront.net/product-logos/d50b96ee-666d-43ce-8d91-d0cbb6f93ffb-rabbitmq.png "RabbitMQ - Tutorial")](https://www.rabbitmq.com/tutorials/tutorial-six-dotnet.html)|

### Some Informations/Tecnologies used :memo:

* Docker
* dotnet core 2.0
* Flurl
* v1.0

* Architecture RabbitMQ:    
![RabbitMQArchitecture](https://www.rabbitmq.com/img/tutorials/python-six.png)

# SetUp!
* You can up a RabbitMQ from a [docker image](https://store.docker.com/images/rabbitmq) using this command:
```
#!sh
λ docker run -p 5672:5672 -p 8080:15672 -d --hostname my-rabbit --name notification-rabbitmq rabbitmq
```

* Restore all the dependencies using Nuget + JFrog
* Then give the play at the macaco


### Advices:
* You can use RabbitMQ Admin to debug your queues created, first of all you need to enable the RabbitMQ Admin:
```
#!sh
λ docker exec -i notification-rabbitmq rabbitmq-plugins enable rabbitmq_management
```

* Then you will can access `localhost:8080` and configure what you need (queues, users, exchanges, etc).
